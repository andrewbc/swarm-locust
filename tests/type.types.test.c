#include <locust/rt.debug.h>

char *test_dlopen()
{

    return NULL;
}

char *test_functions()
{

    return NULL;
}

char *test_failures()
{

    return NULL;
}

char *test_dlclose()
{

    return NULL;
}

char *all_tests() {
    test_prep();

    test_run(test_dlopen);
    test_run(test_functions);
    test_run(test_failures);
    test_run(test_dlclose);

    return NULL;
}

test_main(all_tests);