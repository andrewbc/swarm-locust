#ifndef IFACE_H
#include <locust/type.iface.h>
#endif

TYPE_ID_DEF(iface);

META_FIELDS_DEF(iface) = {
    3, {
    	META_FIELD_DEF(iface, data, ptr),
    	META_FIELD_DEF(iface, meta, ptr),
    	META_FIELD_DEF(iface, iface, ptr)
    }
};

META_DEF(iface, & META_FIELDS_OF(iface), nil);

