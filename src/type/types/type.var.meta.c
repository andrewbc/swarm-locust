#ifndef TYPES_H
#include <locust/type.types.h>
#endif

TYPE_ID_DEF(var);

META_FIELDS_DEF(var) = {
    2, {
    	META_FIELD_DEF(var, type, ptr),
    	META_FIELD_DEF(var, data, ptr)
    }
};

META_DEF(var, & META_FIELDS_OF(var), nil);
