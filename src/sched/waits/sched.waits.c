#ifndef WAITS_H
#include <locust/sched.waits.h>
#endif

#ifndef INSPECT_H
#include <locust/rt.inspect.h>
#endif

#include <stdlib.h>
#include <stdio.h>


// PoSix
#include <sched.h>
#include <unistd.h>
#include <time.h>

extern uint64 threads_count;

void wait_gradual(uint8 rate) {
    // POSIX implementation
    // TODO: Handle other architectures, but x86ish assembly is good for now

    struct timespec req;

    if (rate < 8 && threads_count > 1) {
        // Since we have multiple processors, we can do active spinning in
        // the hopes that someone else will do whatever we're deferring for
        wait_active(rate);
    } else if (rate < 16) {
        // If this hits because threads_count == 1,
        // We're either on a single processor system, or assuming we are, so
        // passive waiting is required, or the other threads won't be able
        // to accomplish whatever we're deferring for
        sched_yield();
    } else {
        rate -= 16;
        req.tv_sec = 0;
        req.tv_nsec = 1 + (rate / (255 - 16)) * 999999998; // wait from 1 nanosecond to 1 second, 4.1841 ms at a time.
        nanosleep(&req, nil);
    }
}




