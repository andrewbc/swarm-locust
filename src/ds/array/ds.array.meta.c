#ifndef ARRAY_H
#include <locust/ds.array.h>
#endif

TYPE_ID_DEF(array);

META_FIELDS_DEF(array) = {
    4, {
    	{.name = "type" , .size = sizeof(ptr), .offset = -((int64)(sizeof(uint64)+sizeof(ptr))), .type = TYPE_ID_OF(ptr)},
    	{.name = "len" , .size = sizeof(uint64), .offset = -((int64)sizeof(uint64)), .type = TYPE_ID_OF(uint64)},
    	{.name = "items" , .size = 0, .offset = 0, .type = TYPE_ID_OF(ptr)},
    	{.name = "front" , .size = sizeof(ptr), .offset = -((int64)1), .type = TYPE_ID_OF(ptr)},
    }
};

META_DECL(array) = { .type = TYPE_ID_OF(array), .size = sizeof(array), .size_varies = false, .name = STRINGIFY(array), .fields = & META_FIELDS_OF(array), .methods = nil };
