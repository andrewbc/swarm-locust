ROOT = $(dir $(CURDIR)/$(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST)))
SRC_DIR = $(ROOT)/src/
BUILD_DIR = build
BUILD_FULL_DIR = $(ROOT)$(BUILD_DIR)

INC_DIRS = -I$(CURDIR)/include
OPT_MODE = -O0 -g3 -DDEBUG

C_MODE = -std=gnu99 -D__TARGET_LINUX
WARNMODE = -W -Wall -Wextra -Winline -Wno-unused-function -Wno-unused-variable -Wno-unused-label -Wno-unused-parameter -Wno-unused-value -ferror-limit=5
C_FLAGS = $(OPT_MODE) -fPIC -fomit-frame-pointer -c $(C_MODE) $(WARNMODE) -static -pthread $(INC_DIRS)
C_FILES = $(shell find $(SRC_DIR) -iname '*.c')
C_OBJ = $(subst $(SRC_DIR), $(BUILD_FULL_DIR)/, $(C_FILES:.c=.c.o))

ASM_FLAGS = $(OPT_MODE) -c -integrated-as
ASM_FILES = $(shell find $(SRC_DIR) -iname '*.s')
ASM_OBJ = $(subst $(SRC_DIR), $(BUILD_FULL_DIR)/, $(ASM_FILES:.s=.s.o))

AR = ar
AR_FLAGS = -r -s

TARGET = liblocust.a

all: clean lib
.PHONY : all

.PHONY : clean
clean:
	rm -rf $(BUILD_FULL_DIR)
	mkdir -p $(dir $(C_OBJ))
	mkdir -p $(dir $(ASM_OBJ))

$(BUILD_FULL_DIR)/%.c.o: $(SRC_DIR)%.c
	$(CC) $(C_FLAGS) -o $@ $<

$(BUILD_FULL_DIR)/%.s.o: $(SRC_DIR)%.s
	$(CC) $(ASM_FLAGS) -o $@ $<

.PHONY : lib
lib: $(ASM_OBJ) $(C_OBJ)
	$(AR) $(AR_FLAGS) $(BUILD_FULL_DIR)/$(TARGET) $(ASM_OBJ) $(C_OBJ)



.PHONY : memcheck
memcheck:
	valgrind --read-var-info=yes --leak-check=yes --leak-check=full --show-leak-kinds=all --track-origins=yes ./build/out

.PHONY : helgrind
helgrind:
	valgrind --tool=helgrind --read-var-info=yes ./build/out

.PHONY : drd
drd:
	valgrind --tool=drd --read-var-info=yes ./build/out

.PHONE : ebnf
ebnf:
	python3.2 -m compiler.parser

.PHONE : cloc
cloc:
	cloc --exclude-dir=build ./runtime

.PHONY : benchmark
benchmark:
	$(CC) $(C_MODE) -Oz -g3 -DDEBUG -static -I/usr/include -I/usr/local/include -I../runtime -L/usr/lib -L/usr/local/lib -o ./profile_custom_oz.out ../runtime/build/locust.a -x c ./profile_custom.c_ -lc
	$(CC) $(C_MODE) -Oz -g3 -DDEBUG -static -I/usr/include -I/usr/local/include -I../runtime -L/usr/lib -L/usr/local/lib -o ./profile_std_oz.out ../runtime/build/locust.a -x c ./profile_std.c_ -lc
	$(CC) $(C_MODE) -O2 -g3 -DDEBUG -static -I/usr/include -I/usr/local/include -I../runtime -L/usr/lib -L/usr/local/lib -o ./profile_custom_o3.out ../runtime/build/locust.a -x c ./profile_custom.c_ -lc
	$(CC) $(C_MODE) -O3 -g3 -DDEBUG -static -I/usr/include -I/usr/local/include -I../runtime -L/usr/lib -L/usr/local/lib -o ./profile_std_o3.out ../runtime/build/locust.a -x c ./profile_std.c_ -lc
	valgrind --tool=callgrind --cache-sim=yes --branch-sim=yes ./profile_custom_oz.out;
	valgrind --tool=callgrind --cache-sim=yes --branch-sim=yes ./profile_std_oz.out;
	valgrind --tool=callgrind --cache-sim=yes --branch-sim=yes ./profile_custom_o3.out;
	valgrind --tool=callgrind --cache-sim=yes --branch-sim=yes ./profile_std_o3.out;

