// This source file is part of the string string library.  This code was
// written by Paul Hsieh in 2002-2010, and is covered by either the 3-clause 
// BSD open source license or GPL v2.0. Refer to the accompanying documentation 
// for details on usage and license.
//
// bstrlib.h
//
// This file is the header file for the core module for implementing the 
// string functions.
//

#define STRING_H

#include <stdarg.h>
#include <string.h>
#include <limits.h>
#include <ctype.h>

#ifndef TYPES_H
#include <locust/type.types.h>
#endif

#ifndef DEBUG_H
#include <locust/rt.debug.h>
#endif

#ifndef MEM_H
#include <locust/rt.mem.h>
#endif

#define string__type_id ((type_id)17)
extern TYPE_ID_DECL(string);
extern META_DECL(string);

typedef byte* string;
typedef byte const * string_const;

// Goals: 
// * simple utf-8 string
// * slice views (no copy)
// * No-copy concatenation
// * all string operations, should return views/iterators (e.g. split)
// * append
// * remove from the front

#define STRING_ERR (-1)
#define STRING_OK (0)
#define STRING_BS_BUFF_LENGTH_GET (0)

uint64
static inline nonnullfunc purestfunc
string_mem_len(uint64 count) {
    count += sizeof(uint64) + sizeof(ptr);
    return count;
}

ptr
static inline nonnullfunc purestfunc
string_mem_front(string self) {
    return self - sizeof(uint64);
}

string
static inline nonnullfunc
string_init(string self, uint64 count) {
    *((uint64*)self) = count;
    self += sizeof(uint64);
    *((ptr*)(self+count)) = self;
    return self;
}

string
static inline
string_create(uint64 count) {
    // strings look like
    // typedef struct {
    //     uint64 len;
    //     uint64 parts;
    //     string_part *front;
    //     string_part *back;
    // } string;
    //
    // string_parts look like
    // typedef struct {
    //     uint64 len;
    //  -> byte bytes[n];
    //     ptr front;
    // } string_part;
    // where n is the length of the string in bytes
    // and a string is a typedef'd ptr whose value is &bytes[0],
    // which front's value also is.
    // So, len and other meta data are behind the ptr.
    ptr ret = mem_get(string_mem_len(count));
    error_goto_if(ret == nil, "NilPtr: ret; mem_get() failure");
    return string_init(ret, count);
  error:
    return nil;
}

void 
static inline nonnullfunc
string_destroy(string self) {
    mem_free(self);
}

uint64
static inline nonnullfunc
string_len(string self) {
    return *(uint64*)(self - sizeof(uint64));
}

ptr
static inline overloaded nonnullfunc
string_index(string self, uint64 i) {
    return self + (i % string_len(self));
}

ptr 
static inline overloaded nonnullfunc
string_index(string self, int64 i) {
    if (i < 0) {
        i = string_len(self) - (__myabs(i) % string_len(self));
    } else {
        i %= string_len(self);
    }
    return self + i;
}

ptr
static inline overloaded nonnullfunc
string_index_unsafe(string self, uint64 i) {
    return self+i;
}

ptr 
static inline overloaded nonnullfunc
string_index_unsafe(string self, int64 i) {
    if (i < 0) {
        i += string_len(self);
    }
    return self+i;
}


// Copy functions
extern string string_deepcopy(string_const b1);
extern int string_assign (string a, string_const b);
extern int string_assignmidstr (string a, string_const b, int left, int len);

// Sustring extraction
extern string string_midstr (string_const b, int left, int len);

// Various standard manipulations
extern int string_concat (string b0, string_const b1);
extern int string_conchar (string b0, char c);
extern int string_catcstr (string b, const char * s);
extern int string_catblk (string b, const void * s, int len);
extern int string_insert (string s1, int pos, string_const s2, unsigned char fill);
extern int string_insertch (string s1, int pos, int len, unsigned char fill);
extern int string_replace (string b1, int pos, int len, string_const b2, unsigned char fill);
extern int string_delete (string s1, int pos, int len);
extern int string_setstr (string b0, int pos, string_const b1, unsigned char fill);
extern int string_trunc (string b, int n);

// Scan/search functions
extern int string_icmp (string_const b0, string_const b1);
extern int string_icmp (string_const b0, string_const b1, int n);
extern int string_iseqcaseless (string_const b0, string_const b1);
extern int string_isstemeqcaselessblk (string_const b0, const void * blk, int len);
extern int string_iseq (string_const b0, string_const b1);
extern int string_isstemeqblk (string_const b0, const void * blk, int len);
extern int string_iseqcstr (string_const b, const char * s);
extern int string_iseqcstrcaseless (string_const b, const char * s);
extern int string_cmp (string_const b0, string_const b1);
extern int string_cmp (string_const b0, string_const b1, int n);
extern int string_instr (string_const s1, int pos, string_const s2);
extern int string_instrr (string_const s1, int pos, string_const s2);
extern int string_instrcaseless (string_const s1, int pos, string_const s2);
extern int string_instrrcaseless (string_const s1, int pos, string_const s2);
extern int string_strchrp (string_const b, int c, int pos);
extern int string_strrchrp (string_const b, int c, int pos);
#define string_strchr(b,c) string_strchrp ((b), (c), 0)
#define bstrrchr(b,c) string_strrchrp ((b), (c), string_len(b)-1)
extern int string_inchr (string_const b0, int pos, string_const b1);
extern int string_inchrr (string_const b0, int pos, string_const b1);
extern int string_ninchr (string_const b0, int pos, string_const b1);
extern int string_ninchrr (string_const b0, int pos, string_const b1);
extern int string_findreplace (string b, string_const find, string_const repl, int pos);
extern int string_findreplacecaseless (string b, string_const find, string_const repl, int pos);

// List of string container functions
struct bstrList {
    int qty, mlen;
    string * entry;
};
extern struct bstrList * string_ListCreate (void);
extern int string_ListDestroy (struct bstrList * sl);
extern int string_ListAlloc (struct bstrList * sl, int msz);
extern int string_ListAllocMin (struct bstrList * sl, int msz);

// String split and join functions
extern struct bstrList * bsplit (string_const str, unsigned char splitChar);
extern struct bstrList * bsplits (string_const str, string_const splitStr);
extern struct bstrList * bsplitstr (string_const str, string_const splitStr);
extern string bjoin (const struct bstrList * bl, string_const sep);
extern int bsplitcb (string_const str, unsigned char splitChar, int pos,
	int (* cb) (void * parm, int ofs, int len), void * parm);
extern int bsplitscb (string_const str, string_const splitStr, int pos,
	int (* cb) (void * parm, int ofs, int len), void * parm);
extern int bsplitstrcb (string_const str, string_const splitStr, int pos,
	int (* cb) (void * parm, int ofs, int len), void * parm);

// Miscellaneous functions
extern int string_pattern (string b, int len);
extern int string_toupper (string b);
extern int string_tolower (string b);
extern int string_ltrimws (string b);
extern int string_rtrimws (string b);
extern int string_trimws (string b);

// <*>printf format functions
extern string string_format (const char * fmt, ...);
extern int string_formata (string b, const char * fmt, ...);
extern int string_assignformat (string b, const char * fmt, ...);
extern int string_vcformata (string b, int count, const char * fmt, va_list arglist);

#define string_vformata(ret, b, fmt, lastarg) { \
string bstrtmp_b = (b); \
const char * bstrtmp_fmt = (fmt); \
int bstrtmp_r = BSTR_ERR, bstrtmp_sz = 16; \
	for (;;) { \
		va_list bstrtmp_arglist; \
		va_start (bstrtmp_arglist, lastarg); \
		bstrtmp_r = bvcformata (bstrtmp_b, bstrtmp_sz, bstrtmp_fmt, bstrtmp_arglist); \
		va_end (bstrtmp_arglist); \
		if (bstrtmp_r >= 0) { /* Everything went ok */ \
			bstrtmp_r = BSTR_OK; \
			break; \
		} else if (-bstrtmp_r <= bstrtmp_sz) { /* A real error? */ \
			bstrtmp_r = BSTR_ERR; \
			break; \
		} \
		bstrtmp_sz = -bstrtmp_r; /* Doubled or target size */ \
	} \
	ret = bstrtmp_r; \
}

typedef int (*bNgetc) (void *parm);
typedef size_t (* bNread) (void *buff, size_t elsize, size_t nelem, void *parm);

// Input functions
extern string string_gets (bNgetc getcPtr, void * parm, char terminator);
extern string string_read (bNread readPtr, void * parm);
extern int string_getsa (string b, bNgetc getcPtr, void * parm, char terminator);
extern int string_assigngets (string b, bNgetc getcPtr, void * parm, char terminator);
extern int string_reada (string b, bNread readPtr, void * parm);

// Stream functions
extern struct bStream * string_stream_open (bNread readPtr, void * parm);
extern void * string_stream_close (struct bStream * s);
extern int string_stream_bufflength (struct bStream * s, int sz);
extern int string_stream_readln (string b, struct bStream * s, char terminator);
extern int string_stream_readlns (string r, struct bStream * s, string_const term);
extern int string_stream_read (string b, struct bStream * s, int n);
extern int string_stream_readlna (string b, struct bStream * s, char terminator);
extern int string_stream_readlnsa (string r, struct bStream * s, string_const term);
extern int string_stream_reada (string b, struct bStream * s, int n);
extern int string_stream_unread (struct bStream * s, string_const b);
extern int string_stream_peek (string r, const struct bStream * s);
extern int string_stream_splitscb (struct bStream * s, string_const splitStr, 
	int (* cb) (void * parm, int ofs, string_const entry), void * parm);
extern int string_stream_splitstrcb (struct bStream * s, string_const splitStr, 
	int (* cb) (void * parm, int ofs, string_const entry), void * parm);
extern int string_stream_eof (const struct bStream * s);

// Safety
#undef gets
#define gets() asserts(0, "bsafe error: gets() is not safe, use bgets.\n")
#undef strncpy
#define strncpy() asserts(0, "bsafe error: strncpy() is not safe, use bmidstr instead.\n")
#undef strncat
#define strncat() asserts(0, "bsafe error: strncat() is not safe, use bstrcat then btrunc\n\tor cstr2tbstr, btrunc then bstrcat instead.\n")
#undef strtok
#define strtok() asserts(0, "bsafe error: strtok() is not safe, use bsplit or bsplits instead.\n")
#undef strdup
#define strdup() asserts(0, "bsafe error: strdup() is not safe, use bstrcpy.\n")

#undef strcpy
#define strcpy() asserts(0, "bsafe error: strcpy() is not safe, use string_copy.\n")
#undef strcat
#define strcat() asserts(0, "bsafe error: strcat() is not safe, use string_append.\n")


























#include <time.h>

/* Safety mechanisms */
#define bstrDeclare(b)               string (b) = NULL; 
#define bstrFree(b)                  {if ((b) != NULL && (b)->slen >= 0 && (b)->mlen >= (b)->slen) { bdestroy (b); (b) = NULL; }}

/* Unusual functions */
extern struct bStream * bsFromBstr (string_const b);
extern string bTail (string b, int n);
extern string bHead (string b, int n);
extern int bSetCstrChar (string a, int pos, char c);
extern int bSetChar (string b, int pos, char c);
extern int bFill (string a, char c, int len);
extern int bReplicate (string b, int n);
extern int bReverse (string b);
extern int bInsertChrs (string b, int pos, int len, unsigned char c, unsigned char fill);
extern string bStrfTime (const char * fmt, const struct tm * timeptr);
#define bAscTime(t) (bStrfTime ("%c\n", (t)))
#define bCTime(t)   ((t) ? bAscTime (localtime (t)) : NULL)

/* Spacing formatting */
extern int bJustifyLeft (string b, int space);
extern int bJustifyRight (string b, int width, int space);
extern int bJustifyMargin (string b, int width, int space);
extern int bJustifyCenter (string b, int width, int space);

/* Esoteric standards specific functions */
extern char * bStr2NetStr (string_const b);
extern string bNetStr2Bstr (const char * buf);
extern string bBase64Encode (string_const b);
extern string bBase64DecodeEx (string_const b, int * boolTruncError);
extern struct bStream * bsUuDecode (struct bStream * sInp, int * badlines);
extern string bUuDecodeEx (string_const src, int * badlines);
extern string bUuEncode (string_const src);
extern string bYEncode (string_const src);
extern string bYDecode (string_const src);

/* Writable stream */
typedef int (* bNwrite) (const void * buf, size_t elsize, size_t nelem, void * parm);

struct bwriteStream * bwsOpen (bNwrite writeFn, void * parm);
int bwsWriteBstr (struct bwriteStream * stream, string_const b);
int bwsWriteBlk (struct bwriteStream * stream, void * blk, int len);
int bwsWriteFlush (struct bwriteStream * stream);
int bwsIsEOF (const struct bwriteStream * stream);
int bwsBuffLength (struct bwriteStream * stream, int sz);
void * bwsClose (struct bwriteStream * stream);

/* Security functions */
#define bSecureDestroy(b) {                                             \
string bstr__tmp = (b);                                                \
    if (bstr__tmp && bstr__tmp->mlen > 0 && bstr__tmp->data) {          \
        (void) memset (bstr__tmp->data, 0, (size_t) bstr__tmp->mlen);   \
        bdestroy (bstr__tmp);                                           \
    }                                                                   \
}
#define bSecureWriteProtect(t) {                                                  \
    if ((t).mlen >= 0) {                                                          \
        if ((t).mlen > (t).slen)) {                                               \
            (void) memset ((t).data + (t).slen, 0, (size_t) (t).mlen - (t).slen); \
        }                                                                         \
        (t).mlen = -1;                                                            \
    }                                                                             \
}
extern string bSecureInput (int maxlen, int termchar, 
                             bNgetc vgetchar, void * vgcCtx);

