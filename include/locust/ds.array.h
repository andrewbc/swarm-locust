#define ARRAY_H

#ifndef TYPES_H
#include <locust/type.types.h>
#endif

#ifndef DEBUG_H
#include <locust/rt.debug.h>
#endif

#ifndef MEM_H
#include <locust/rt.mem.h>
#endif

#define array__type_id ((type_id)27)
extern TYPE_ID_DECL(array);
extern META_DECL(array);

typedef ptr array;

uint64
static inline nonnullfunc purestfunc
array_mem_len(size_t type_size, uint64 count) {
    count = type_size * count;
    count += sizeof(meta const * const) + sizeof(uint64) + sizeof(ptr);
    return count;
}

ptr
static inline nonnullfunc purestfunc
array_mem_front(array self) {
	return self - (sizeof(uint64) + sizeof(meta const *));
}

array
static inline nonnullfunc
array_init(array restrict self, meta const * const restrict type, uint64 count) {
    *((meta const **)self) = type;
	self += sizeof(meta const * const);
	*((uint64*)self) = count;
	self += sizeof(uint64);
	*((ptr*)(self+count)) = self;
	return self;
}

array
static inline nonnullfunc
array_create(meta const * const type, uint64 count) {
	// arrays look like
    // typedef struct {
    //     meta const * type;
    //     uint64 len;
    //  ->  byte items[n];
    //     ptr front;
    // } array;
    // where n is the length of the array in bytes
    // and an array is a typedef'd ptr whose value is &contents[0],
    // which front's value also is.
    // So, length and meta data are behind the ptr.
	ptr ret = mem_get(array_mem_len(type->size, count), type);
	error_goto_if(ret == nil, "NilPtr: ret; mem_get() failure");
	return array_init(ret, type, count);
  error:
  	return nil;
}

static inline nonnullfunc
meta const *
array_type(array self) {
	return *(meta const * const *)(self - (sizeof(uint64) + sizeof(meta const * const)));
}

uint64
static inline nonnullfunc
array_len(array self) {
	return *(uint64*)(self - sizeof(uint64));
}

void
static inline nonnullfunc
array_deinit(array self) {
	uint64 count = array_len(self);
	meta const *type = array_type(self);
	uint64 byte_count = array_mem_len(type->size, count);
	mem_set(self, byte_count, 0);
	*(meta const **)(self - (sizeof(uint64) + sizeof(meta const *))) = & META_OF(byte);
	*(uint64*)(self - sizeof(uint64)) = byte_count;
}

ptr
static inline overloaded nonnullfunc
array_index(array self, uint64 i) {
	i *= array_type(self)->size;
	i %= array_len(self);
	return (ptr)(((uintptr_t)self) + i );
}

ptr 
static inline overloaded nonnullfunc
array_index(array self, int64 i) {
	uint64 ii = (((uint64)(__myabs(i))) * array_type(self)->size) % array_len(self);
	if (i < 0) {
		ii = array_len(self) - ii;
	}
    return (ptr)(((uintptr_t)self) + ii);
}

ptr
static inline overloaded nonnullfunc
array_index_unsafe(array self, uint64 i) {
	return (ptr)(((uintptr_t)self) + i );
}
